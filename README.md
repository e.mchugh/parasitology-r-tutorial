# Intro to R tutorial November 2020 for Ralph and McFadden labs

This repo has material I put together for a short tutorial (~2.5 h) presented on Zoom on 12/11/2020. The tutorial works through how to create two plots from data collected from *Plasmodium falciparum*: one from an experiment on knockdown of an exported protein **REX1** (`maurers_clefts.csv`) [published here](https://doi.org/10.1111/mmi.13201), and another looking at delayed death after drug treament in various conditions (`control_egress.csv` and `death_egress.csv`).  

The tutorial is aimed at absolute beginners and covers:

* Reading data (.csv files) provided by lab members into R
* Tidying the data using `dplyr` functions (incl. how to identify "messy" data)
* Plotting data using `ggplot2`